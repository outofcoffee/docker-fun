#!/bin/bash
set -e

WORKING_DIR="$( pwd )"

cd ${WORKING_DIR}/.. && ./gradlew clean

cd ${WORKING_DIR}

docker rm wildflyredis_wildfly_1 || echo "No wildfly container to remove"
docker rm wildflyredis_redis_1 || echo "No redis container to remove"

docker rmi wildflyredis_wildfly:latest || echo "No wildfly image to remove"
