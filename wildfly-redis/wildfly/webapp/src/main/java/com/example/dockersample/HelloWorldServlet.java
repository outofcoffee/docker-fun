package com.example.dockersample;

import redis.clients.jedis.Jedis;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HelloWorldServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private Jedis jedis;

    @Override
    public void init() throws ServletException {
        jedis = new Jedis("redis", 6379);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)  
            throws ServletException, IOException {

        String message = String.format("This page has been hit %s times.", jedis.incr("counter"));
        resp.getOutputStream().write(message.getBytes());
    }
}