#!/bin/bash
set -e

WORKING_DIR="$( pwd )"

cd ${WORKING_DIR}/.. && ./gradlew build

cd ${WORKING_DIR}
/usr/local/bin/docker-compose up
