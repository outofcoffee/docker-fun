# Docker Machine

If you are using *docker-machine*, here is a script to add or replace an entry in /etc/hosts with the IP of the active Docker Machine.

```
./docker-machine-host.sh
```

# Other Docker examples

Example of node.js, Python, nginx + redis using Docker.

Examples (all elements are separate containers but connected to eachother using Docker links via Docker Compose):

* Python web app connected to a redis DB
* Node web app connected to a redis DB
* Java web app connected to redis DB
* stand up a multiple node Java webapp, on WildFly, connected to a redis DB, load-balanced behind an nginx reverse proxy

## To do:

* stand up a nodejs web app (using express) connected to a redis DB, continually tested via Jenkins
* use etcd and configd to update nginx configuration based on scale of webapp cluster

# Pre-requisites

Install docker

``sudo yum install docker``

Install docker compose

``sudo yum install docker-compose``

Set up docker user:

```
$sudo groupadd docker

$ sudo gpasswd -a ${USER} docker
Adding user pete to group docker

$ sudo service docker restart
Redirecting to /bin/systemctl restart  docker.service

```

*Important:* log out and log in again after group changes.

# Running

Ensure dockerd is running:

``sudo docker -d``

Then test docker:

``docker ps``

Bring up containers:

``docker-compose up``