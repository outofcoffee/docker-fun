#!/bin/bash

#############################################
# Author: Pete Cornish
#
# Add or replace an entry in /etc/hosts with
# the IP of the active Docker Machine.
#
# Requires docker-machine command to be
# installed and on the PATH. A machine should
# have been created and started.
#############################################

set -e

#############################################
# Settings
#############################################

# your desired hostname
DOCKER_VM_HOSTNAME="dockerhost"

##specify the docker-machine name
#DOCKER_MACHINE_NAME="local"

#############################################
# Constants
#############################################

# your old hosts file in case things go wrong
HOSTS_FILE_BACKUP="/var/tmp/hosts.bak"

# modified copy
HOSTS_FILE_TMP="/tmp/hosts.docker"

#############################################
# Script
#############################################

if [[ "" == "${DOCKER_MACHINE_NAME}" ]]; then
	# attempt to get active docker machine
	DOCKER_MACHINE_NAME="$( docker-machine active )"
	if [[ "$?" != "0" ]]; then
		echo "Error getting active Docker Machine - is it running?"
		exit 1
	fi
fi

echo ""
echo "Using Docker Machine '${DOCKER_MACHINE_NAME}'"

# obtain machine IP
DOCKER_MACHINE_IP="$( docker-machine ip ${DOCKER_MACHINE_NAME} )"

if [[ "$?" != "0" ]]; then
	echo "Error getting IP for Docker Machine '${DOCKER_MACHINE_NAME}' - is it running?"
	exit 1
fi

# imperfect regex to validate IP format (will match 0.0.0.0 - 999.999.999.999)
IP_VALID="$( echo ${DOCKER_MACHINE_IP} | grep -E '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' )"

if [[ "${DOCKER_MACHINE_IP}" != "${IP_VALID}" ]]; then
	echo "Invalid IP for Docker Machine '${DOCKER_MACHINE_NAME}' IP - is it running?"
	exit 1
else
	echo ""
	echo "IP for Docker Machine '${DOCKER_MACHINE_NAME}': ${DOCKER_MACHINE_IP} - appears to be valid"
fi

# create host name entry
HOSTS_FILE_ENTRY="${DOCKER_MACHINE_IP}  ${DOCKER_VM_HOSTNAME}"

# backup
echo ""
echo "Backing up hosts file to ${HOSTS_FILE_BACKUP}"
cp /etc/hosts ${HOSTS_FILE_BACKUP}

# modify copy of hosts
grep -v "${DOCKER_VM_HOSTNAME}" /etc/hosts > ${HOSTS_FILE_TMP}
printf "${HOSTS_FILE_ENTRY}\n" >> ${HOSTS_FILE_TMP}

# show updated copy
echo ""
echo "New hosts file:"
echo "----------------------------------------"
cat ${HOSTS_FILE_TMP}
echo "----------------------------------------"

# update hosts
echo ""
echo "Attempting to update hosts file..."
sudo cp ${HOSTS_FILE_TMP} /etc/hosts

if [[ "$?" == "0" ]]; then
	echo ""
	echo "Updated hosts file with entry:"
	echo "${HOSTS_FILE_ENTRY}"
else
	echo "Error updating hosts file"
	exit 1
fi

# clean up
echo ""
echo "Removing temporary file ${HOSTS_FILE_TMP}"
rm ${HOSTS_FILE_TMP}
