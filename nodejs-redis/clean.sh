#!/bin/bash
set -e

docker rm nodejsredis_node_1 || echo "No container to remove"

docker rmi nodejsredis_node:latest || echo "No image to remove"

